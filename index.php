<?php
  require 'lib/sanitize.php';
  require "authenticate.php";
  require 'db_credentials.php';
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Mock Draft</title>
</head>
<body>
<p>
<ul>
  <?php if ($login): ?>
    <li><a href="logout.php">Logout</a></li>
    </a>
  <?php else: ?>
    <li><a href="login.php">Login</a></li>
    <li><a href="register.php">Registrar-se</a></li>
    <a href="makelogin.php">
      <button aria-label="Feito" class="btn btn-sm btn-success" type="button">
        <span class="glyphicon glyphicon-ok"></span> Mock
      </button>
    </a>
  <?php endif; ?>
</ul>
</p>
<h1> MOCK DRAFT </h1><br>
  <h3> O que é o Draft? </h3><br>
  <h5></p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lacinia, ipsum nec pulvinar aliquam, elit turpis porta dolor,
    id sodales sapien massa sodales diam. Duis dictum dui id sapien facilisis vehicula. Maecenas suscipit vestibulum purus,
    eget consectetur nisi pellentesque sagittis. Vivamus vitae posuere quam. Morbi arcu sem, dapibus ac quam ac, euismod condimentum erat.
    In mollis odio augue, sed lacinia ipsum iaculis sed. Aenean pharetra vestibulum elit. Nunc interdum metus in mattis rutrum. Sed non eros et
    magna efficitur luctus vitae vel nisi. Vivamus ut leo ornare, aliquam nisi quis, molestie ante.</p>

    </p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lorem eu quam lacinia porttitor. Cras faucibus hendrerit molestie.
    Curabitur congue scelerisque semper. Vestibulum et augue blandit, auctor nulla sit amet, scelerisque ante. Nullam ultricies felis id sapien commodo congue.
    Vivamus efficitur magna vitae porta porta. Curabitur feugiat facilisis dui nec viverra. In ac risus non augue mollis egestas id at dolor.
    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras a interdum nulla. Suspendisse et elit mi.
    Sed semper mauris vel auctor laoreet. In bibendum, erat et elementum luctus, lacus lacus lacinia felis, id rhoncus velit justo eget magna.
    Etiam dapibus, libero hendrerit condimentum euismod, ex sem porttitor lectus, eu pharetra velit nibh bibendum dui.</p>

    </p>Mauris auctor nulla sit amet nulla viverra ullamcorper. Donec in lectus id dolor luctus tristique et eu arcu. Cras varius posuere elit,
    ac efficitur justo venenatis eu. Suspendisse ac ex nec eros tempus vehicula tincidunt sed nulla. Mauris eu luctus sapien. Sed eleifend justo nibh,
    in lobortis lacus malesuada ac. Ut ornare iaculis urna, eget cursus massa bibendum in. Phasellus convallis eget nunc nec facilisis.
    Aliquam molestie sagittis scelerisque. Donec vitae semper arcu. Morbi non dictum mauris. Aliquam erat volutpat.</p>

    </p>Aenean accumsan leo eget leo suscipit, vel egestas est ullamcorper. Fusce sagittis rhoncus ante non aliquam. Phasellus faucibus sapien augue.
    Sed quis semper ipsum. Donec pretium a turpis sed vehicula. Vestibulum sollicitudin euismod ex ut sagittis. Morbi maximus, arcu et dapibus posuere,
    sapien dolor blandit nibh, at finibus odio urna in justo. In aliquam, elit vitae dignissim posuere, lorem odio semper metus, ut dictum lacus nibh at eros.
    Aenean a mauris porta, mollis nisl eget, tempus odio.</p>

    <p>FAÇA AQUI SEU MOCK DRAFT AO CLICKAR NO BOTÃO MOCK</p>
    <a href="mock.php">
      <button aria-label="Feito" class="btn btn-sm btn-success" type="button">
        <span class="glyphicon glyphicon-ok"></span> Mock
      </button>
    </a>
</h5>
</body>
</html>
