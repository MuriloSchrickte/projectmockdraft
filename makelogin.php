<?php
  require "authenticate.php";
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Mock Draft</title>
</head>
<body>

  <h1> MOCK DRAFT </h1><br>
    <h5><p>Você não está logado ainda, por favor faça seu login ou registre-se.
  </h5>
</p>
<ul>
  <?php if ($login): ?>
    <li><a href="logout.php">Logout</a></li>
  <?php else: ?>
    <li><a href="login.php">Login</a></li>
    <li><a href="register.php">Registrar-se</a></li>
  <?php endif; ?>
</ul>
</p>

</body>
</html>
