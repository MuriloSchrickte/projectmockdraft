<?php
  require 'lib/sanitize.php';
  require "authenticate.php";
  require "db_credentials.php";

  $conn = mysqli_connect($servername,$username,$db_password,$dbname2);
  if (!$conn) {
    die("Problemas ao conectar com o BD!<br>".
         mysqli_connect_error());
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["novo-time"])) {

      $time = sanitize($_POST["novo-time"]);
      $time = mysqli_real_escape_string($conn, $time);
      $conference = ($_POST["conference"]);
      $division = ($_POST["division"]);

      $sql = "INSERT INTO $table_teams (name, conference, division)
              VALUES ('$time', '$conference', '$division')";

      if(!mysqli_query($conn,$sql)){
        die("Problemas para inserir nova tarefa no BD!<br>".
             mysqli_error($conn));
      }
    }
  }

  elseif ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET["act"]) && isset($_GET["id"])) {
      $sql = "";

      $id = sanitize($_GET['id']);
      $id = mysqli_real_escape_string($conn, $id);

      if($_GET["act"] == "add"){
        $sql = "UPDATE $table_teams
                WHERE id=" . $id;
      }
      elseif($_GET["act"] == "remove"){
        $sql = "DELETE FROM $table_teams
                WHERE id=" . $id;
      }

      if ($sql != "") {
        if(!mysqli_query($conn,$sql)){
          die("Problemas para executar ação no BD!<br>".
               mysqli_error($conn));
        }
      }
    }
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["novo-jogador"])) {

      $player = sanitize($_POST["novo-jogador"]);
      $player = mysqli_real_escape_string($conn, $player);
      $position = ($_POST["position"]);
      $college = ($_POST["college-jogador"]);
      $college = mysqli_real_escape_string($conn, $college);

      $sql = "INSERT INTO $table_prospects(name, position, college)
              VALUES ('$player', '$position', '$college')";

      if(!mysqli_query($conn,$sql)){
        die("Problemas para inserir nova tarefa no BD!<br>".
             mysqli_error($conn));
      }
    }
  }

  elseif ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET["act"]) && isset($_GET["id"])) {
      $sql = "";

      $id = sanitize($_GET['id']);
      $id = mysqli_real_escape_string($conn, $id);

      if($_GET["act"] == "add"){
        $sql = "UPDATE $table_prospects
                WHERE id=" . $id;
      }
      elseif($_GET["act"] == "remove"){
        $sql = "DELETE FROM $table_prospects
                WHERE id=" . $id;
      }

      if ($sql != "") {
        if(!mysqli_query($conn,$sql)){
          die("Problemas para executar ação no BD!<br>".
               mysqli_error($conn));
        }
      }
    }
  }

  $sql = "SELECT * FROM $table_teams";
  if(!($table_teams_set = mysqli_query($conn,$sql))){
    die("Problemas para carregar tarefas do BD!<br>".
         mysqli_error($conn));
  }

  $sql = "SELECT * FROM $table_prospects";
  if(!($table_prospects_set = mysqli_query($conn,$sql))){
    die("Problemas para carregar tarefas do BD!<br>".
         mysqli_error($conn));
  }

mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Mock Draft</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/bootstrap.css">
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
</head>
<body>
<p>
<ul>
  <?php if ($login): ?>
    <li><a href="logout.php">Logout</a></li>
  <?php else: ?>
    <li><a href="login.php">Login</a></li>
    <li><a href="register.php">Registrar-se</a></li>
  <?php endif; ?>
</ul>
</p>
<h1> MOCK DRAFT </h1><br>
  <h3> MOCK DRAFT NFL </h3><br>
  <div class="container">
    <div class="row">
      <div class="col">
        <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST">
          <div class="form-group">
            <label class="sr-only" for="inputTime">Inserir novo time</label>
            <input required type="text" name="novo-time" id="novoTime" placeholder="Novo Time">
            <select required name="conference" id="conference">
            <datalist id="conference">
              <option value="AFC">AFC</option>
              <option value="NFC">NFC</option>
            </datalist>
          </select>
            <select required name="division" id="division">
            <datalist id="division">
              <option value="North">North</option>
              <option value="South">South</option>
              <option value="East">East</option>
              <option value="West">West</option>
            </datalist>
          </select>
          </div>
        </form>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-list"></span>
              Times
            </h3>
          </div>
          <div class="panel-body">

        <?php if(mysqli_num_rows($table_teams_set) > 0): ?>
          <?php while($table_teams = mysqli_fetch_assoc($table_teams_set)): ?>
                <?php echo $table_teams["id"]. " " . $table_teams["name"]. " " . $table_teams["conference"]. " " . $table_teams["division"] ?>
                <a class="btn-remove-teams" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $table_teams["id"]  . "&" . "act=remove" ?>">
                  <button aria-label="Remover" class="btn btn-sm btn-danger" type="button">
                    <span class="glyphicon glyphicon-trash">Remover</span>
                  </button><br>
                </a>
              <?php endwhile; ?>
            <?php else: ?>
              Sem Times no Momento
            <?php endif; ?>
      </div>
</div>
</div>

      <div class="col">
        <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST">
          <div class="form-group">
            <label class="sr-only" for="inputProspects">Inserir novo jogador</label>
            <input required type="text" name="novo-jogador" id="novoJogador" placeholder="Novo Jogador">
            <select required name="position" id="position">
            <datalist id="position">
              <option value="QB">QB</option>
              <option value="RB">RB</option>
              <option value="WR">WR</option>
              <option value="FB">FB</option>
              <option value="TE">TE</option>
              <option value="C">C</option>
              <option value="OG">OG</option>
              <option value="OT">OT</option>
              <option value="DT">DT</option>
              <option value="DE">DE</option>
              <option value="EDGE">EDGE</option>
              <option value="LB">LB</option>
              <option value="CB">CB</option>
              <option value="S">S</option>
              <option value="PK">PK</option>
              <option value="P">P</option>
              <option value="LS">LS</option>
              <option value="KR/PR">KR/PR</option>
            </datalist>
          </select>
            <input required type="text" name="college-jogador" id="collegeJogador" placeholder="Colégio Jogador">
          </div>
        </form>


        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-list"></span>
              Jogadores
            </h3>
          </div>
          <div class="panel-body">

                <?php if(mysqli_num_rows($table_prospects_set) > 0): ?>
                  <?php while($table_prospects = mysqli_fetch_assoc($table_prospects_set)): ?>
                        <?php echo $table_prospects["id"]. " " . $table_prospects["name"]. " " . $table_prospects["position"]. " " . $table_prospects["college"] ?>
                        <a class="btn-remove-teams" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $table_prospects["id"]  . "&" . "act=remove" ?>">
                          <button aria-label="Remover" class="btn btn-sm btn-danger" type="button">
                            <span class="glyphicon glyphicon-trash">Remover</span>
                          </button><br>
                        </a>
                      <?php endwhile; ?>
                    <?php else: ?>
                      Sem Prospectos no momento
                    <?php endif; ?>
              </div>
        </div>
        </div>
      </div>
    </div>
</body>
</html>
